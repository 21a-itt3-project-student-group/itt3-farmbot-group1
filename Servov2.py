from RPi import GPIO
from time import sleep
from ina219 import INA219
from ina219 import DeviceRangeError




class Gripper:
    def __init__(self):

        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(11, GPIO.OUT)  # setting pin 11 to OUT
        self.ina = INA219(0.1, 0.4)  # "Shunt_ohms" set to 0.1 and "MAX_expected_amps" set to 0.4mA
        self.ina.configure(voltage_range=self.ina.RANGE_16V, gain=self.ina.GAIN_AUTO, bus_adc=self.ina.ADC_128SAMP,
                           shunt_adc=self.ina.ADC_128SAMP)
        self.p = GPIO.PWM(11, 50)  # sets pin 11 to 50hz frequency
        self.p.start(0)
        self.y = 0
        # Start servo_motor at duty cycle 0
        self.servo_loop()
        self.servo_letgo()

    def servo_loop(self):
        try:
            while True:
                breaker = False
                for x in range(95, 120):
                    dc = 2 + (x / 18)  #DC = dutycyle pr. degree
                    self.p.ChangeDutyCycle(dc)
                    #print(dc)
                    sleep(0.1)
                    current_reading = self.ina.current()  # Measure current from INA219
                    print(current_reading)
                    self.y +=1
                    print(self.y)
                    if float(current_reading) >= 350.0 and self.y > 5:  # Servo motor uses 300mA+. Prepare to break loop
                        print("uses to much current!")
                        #self.p.stop(dc)
                        breaker = True
                          # Servo motor stops at around the current duty cycle
                        break

                if breaker:  # If breaker is True, break out of main loop.
                    print("Gripper force calibrated")
                    break
                else:
                    self.p.start(0)
                
                
                
                #  goes back to start position. Create new function for how long to grip ojbect

        except DeviceRangeError as e:
            # Current out of device range with specified shunt resistor
            print(e)
            GPIO.cleanup()

        except KeyboardInterrupt:
            GPIO.cleanup()
            
    def servo_letgo(self):
        while True:
            user_choice = input("press 'y' to release tomato: ")
            if user_choice == "y":
                self.p.start(0)
                self.p.stop()
                break
            else:
                pass
                


if __name__ == "__main__":
    Controller = Gripper()
