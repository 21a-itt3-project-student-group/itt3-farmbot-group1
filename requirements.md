# Requirements

- 3D printed parts for gripper.
- 2 sensors will be use. 1. Measuring current/resistance, 2. measuring distance
- Raspberry Pi or Arduino.
- Acces to farmbot to make our tests. 
- A vegetable to test the gripper on it. 
