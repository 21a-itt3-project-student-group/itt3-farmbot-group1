# **PROJECT FARMBOT**

 # WW34 Project Start-up
 - [x] Deciding group members and what project to make.

 # WW35 Project Start-up
 - [x] Block diagram
 - [x] Use case
 - [x] Brainstorm
 - [x] Gitlab issues

 # WW36 Project planning
 - [x] Investigate what type of sensors to use.
 - [x] Brainstorm
 - [x] Discuss use case
 - [x] Make a Use Case model

 # WW37 Project planning & Executing
 - [x] Investigate what sensors to use. 
 - [x] System brainstorm.
 - [x] Risk Analysis.
 - [x] Discuss about Hardware/Prototype.

 # WW38 Project executing & controlling
 - [x] Software start up
 - [x] Risk Analysis
 - [x] Hardware/prototype
 - [x] Milestones
 
 # WW39 Project executing & controlling
 - [x] Software start up
 - [x] Researching how to use the gripper parts.
 - [x] Researching how to use the torque, current sensor.
 - [x] Milestones
 
 # WW40 Project controlling & closing
 - [x] Project diagrams.
 - [x] Hardware and software designing.
 - [x] Project testing and overeiew over your project.
 - [x] Starting to write project report

 # WW41 Project closing
 - [x] Finishing the project exam report
 - [x] Hand in the project exam report

 # WW42 Project tests
 - [x] Test the gripper for last time

 # WW43 Project controlling & closing
 - [x] Project presentation

 # WW44 Unified Process - Inception phase
 - [x] The student must understand the Unified Process phases.
 - [x] The student may have the ability to prioritize project tasks using the Unified Process phases.
 - [x] The student should be able to prioritize tasks using Unified Process phases.
 - [x] The student must understand the iterative development.
 - [x] The student may be able to use iterative development.
 - [x] The student should be able to prioritize tasks using iterative development.
 - [x] The student should be able to make project plan and schedule.

 # WW45 Unified Process - Inception phase & Elaboration phase
 - [x] Project brainstorming
 - [x] Project requirements
 - [x] Project planning
 - [x] Project schedule
 - [x] Project use case

 # WW46 Unified Process - Inception & Elaboration phases
 - [x] Project brainstorming
 - [x] Project requirements
 - [x] Project planning
 - [x] Project schedule
 - [x] Project use case
 - [x] Presentation

 # WW47 Unified Process - Elaboration & Construction phases
 - [x] Risk analysis
 - [x] Milestones
 - [x] All software functions
 - [x] Starting to write second report for exam

 # WW48 Unified Process - Elaboration & Construction phases
 - [x] Risk analysis
 - [x] Milestones
 - [x] All software functions
 - [x] Working on report

 # WW49 
 - [x] Hand in of the Project report

 # WW50 
 - [x] Final tests
