# ITT3-Farmbot-Group1

**Group Members**
- Can Gabriel Bekil
- Bogdan Vanghelie
- Lukasz Zwak

**Idea Of Project**
- Idea or the purpose of the project is to make a **Hand** that can be used to harvest or grab the vegetables without damaging it.

![Use casses](usecase.png)

**Brainstorm Suggestions**  
1. The form of the hand will be like human hand but with 3 fingers.
2. This design will grab the vegetables gently. And later it will put the vegetables where is needed.  
3. The gripper hand will have a sensor that will measure how soft is the vegetables and it will not squeeze it.
4. The sensor from the gripper can detect how big will be the vegetables and grab it gently without squeezing it.
5. To add new sensor to detect the location of the vegetables.
![Brainstorm Diagram](Brainstorm.png)

**Team Building**
- The team member know each other from school and the part time job. We know what to except from each other, so we know our strong points and weaknesses. So we can divide the work properly and efficiently. 

**Project Management**
- We decided to not elect a team leader so that everyone has equal rights and responsibilities.
- Every week we will discuss in a meeting what's done and what needs to be finished or started the current week.
- We will talk about every problem, mistake or misunderstanding so we can correct them and everyone can propose changes about the project, which will be discussed.
![Project Management](project_managment.png)


**The Gripper Working Video**
- ![](The_Gripper.mp4)

**PROJECT Plan FARMBOT**
- https://gitlab.com/21a-itt3-project-student-group/itt3-farmbot-group1/-/blob/main/projectplan.md

**Project Managment**
- https://gitlab.com/21a-itt3-project-student-group/itt3-farmbot-group1/-/blob/main/project_managment.md
