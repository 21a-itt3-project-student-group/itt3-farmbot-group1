# Software Testing Plan

- The testing will be done on "Cherry Tomatoes".
- The gripper will be tested to make sure that it does not destroy the tomato while gripping it.
- The gripper will be used with the python code.
- The gripper code will be runnned on the Raspberry Pi.
- The code on the Raspberry Pi will be able to read current values from the sensor on gripper which will give us the torque value.
- The current sensor will be connected to Raspberry Pi by I2C connection. And the gripper motor will be connected by gpio.
- The code will also test the ultrasonic sensor, so we make sure that it can measure the distance between the vegetable and the gripper.
