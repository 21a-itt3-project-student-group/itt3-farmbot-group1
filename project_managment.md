# Project Management
 
 # Group Members
 - Can Gabriel Bekil
 - Bogdan Vanghelie
 - Lukasz Zwak
 
 # Purpose
 - Idea or the purpose of the project is to make a Hand that can be used to harvest or grab the vegetables without damaging it.

 # Goals
 - To make fully functioning FarmBot "Hand" that can detect the distance between the gripper and grab the vegetables, and to improve it with teamwork.

 # Schedule
 - Every Tuesdays from 8:15 to 15:30. Sometimes with overtime, depends on workload.

 # Organization
 - We decided to not elect a team leader so that everyone has equal rights and responsibilities.
 - Every week we will discuss in a meeting what's done and what needs to be finished or started the current week.
 - We will talk about every problem, mistake or misunderstanding so we can correct them and everyone can propose changes about the project, which will be discussed.
 - The team member know each other from school and the part time job. We know what to except from each other, so we know our strong points and weaknesses. So we can divide the work properly and efficiently.



 # BrainStorm ideas
 - The form of the hand will be like human hand but with 3 fingers.
 - This design first will grab the cherry Tomatoes gently. And later it will put the cherry tomatoes where is needed.
 - Later will make test on the vegetable. 
 - The gripper hand will have a sensor that will measure how soft is the tomato or vegetable and it will not squeeze it.
 - The sensor from the gripper can detect how big will be the tomato and grab it gently without squeezing it.
 - And will add the secondary sensor(ultrasonic) that can measure the distance between the gripper hand and the vegetable.


 # Budget and resources
 - From own inventory and E-lab

 # Risk assessment

 **Potential problems**
 - Signals processing is not accurate enough.
 - The vegetables can be too soft and get damaged.  
 - The vegetables can be too small. 
 - Breaking parts of the farmbot somehow.
 - The sensor might not measure correctly and can destroy the vegetables.
 - The ultrasonic sensor might not correctly measure the distance between the gripper and the vegetables.

 **Possible solution for these problems**
 - Make better python scripts to adjust the sensors to have more accurate meassurements.
 - Adding rubber or soft material to the gripper so it can grab without damaging the cherry tomatoes.
 - Is possible to set the ultrasonic sensor to detect the vegetable and when it starts to grab it can detect the distance between the floor so it doesn't crush the gripper to the floor.  

 **Probability for problem to occur**
 - Depends on the sensor measurement accuracity
 - Cherry tomatoes size will be unpredictable
 - Cherry tomatoes will be maybe really soft
 - High possibility
 - Low chance


 # Stakeholders
 [An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse  interests in the project.

 Possible stakeholders  

    • Potential farmers that will use it on their farms

    • The farmbot company....

 A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.  
 This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

 TBD: This section is to be completed by the students.  
 TBD: Insert your relevant external stakeholder.

 # Communication
 - We mainly are going to use discord as our main communication way.

 # References
 - Common project group on gitlab.com
 - Project owners



