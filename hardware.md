We would require the following hardware:

- Raspberry Pi or Arduino

- Farmbot

- Current/resistence sensor 

- Distance sensor


| Sensor name | Cost | Pros | Cons | Link |
| ------ | ------ | ------ | ------ | ------ |
| GY-219 "INA219" I2C | 27,00 DKK | Very Cheap | Not very accurate |https://arduinotech.dk/shop/gy-219-ina219-i2c-interface-bi-directional-currentpower-monitoring-sensor-module |
| ULTRASONIC DISTANCE SENSOR - HC-SR04 | 34,00 DKK | It's cheap and easy to use | It's not that accurate | https://let-elektronik.dk/shop/1440-afstand--bevaegelse/15569-ultrasonic-distance-sensor---hc-sr04/ |
